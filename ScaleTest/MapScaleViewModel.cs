﻿using System;
using System.ComponentModel;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace ScaleTest
{
    public class MapScaleViewModel : INotifyPropertyChanged
    {
        private int _absoluteScaleDenominator;

        public int MapScaleDenominator
        {
            get { return _absoluteScaleDenominator; }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void NotifyPropertyChanged(String info)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(info));
            }
        }

        public MapScaleViewModel() { }

        public void GetMapScale_ExtentChanged(object sender, ESRI.ArcGIS.Client.ExtentEventArgs e)
        {

            ESRI.ArcGIS.Client.Map myMap = (ESRI.ArcGIS.Client.Map)sender;

            // WGS 84 sphere radius in meters.
            double earthRadius = 6378137;

            // Get diagonal map length in pixels using Pythagorean theorum.
            double a = myMap.ActualWidth;
            double b = myMap.ActualHeight;
            double c = Math.Sqrt(Math.Pow(a, 2) + Math.Pow(b, 2));

            // Pixels to meters based on a normal screen resolution of 96 points per inch (ppi) (c / 96 * .0254).
            double cMeters = (c / 96) * .0254;

            // Use values from the new extent to calculate Euclidian distance based on an Earth sphere in meters. This is based on a perfect sphere representation of the Earth.
            double sphereDistMeters = Math.Sqrt(Math.Pow(e.NewExtent.XMax - e.NewExtent.XMin, 2) + Math.Pow(e.NewExtent.YMax - e.NewExtent.YMin, 2)) * (Math.PI / 180) * earthRadius;

            // Calculate the scale denominator and raise the PropertyChanged event.
            _absoluteScaleDenominator = (int)Math.Round(sphereDistMeters / cMeters);
            NotifyPropertyChanged("MapScaleDenominator");
        }
    }
}
